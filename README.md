# Namma Care - latest (v1.2.0)
## Self Care | General Symptoms Assessment | Health News

Namma care is a cloud-enabled, mobile-ready, offline compatible General Symptoms Checker and Self Care toolkit.

## Features
- Basic Health fitness Tips & News
- General Symptoms Assessment Checker
- Export PDF Assessment report
- Connect with Nearby HCPs
- Latest real-time Vaccination Alerts

## Installation

Download latest NodeJS
Download Latest Expo SDK
Download Expo Go app in Android/ iOS

Clone this repo

Install the dependencies and devDependencies and start the server.

```sh
cd Namma Care
npm i
expo start
```

## APK Build File

| Storage | Namma care Android app (v1.2.0) |
| ------ | ------ |
| Google Drive | https://drive.google.com/file/d/1PB2rXmt8lEeKtTVTgvbLjm6satP8h-4m/view?usp=sharing |

